CREATE TABLE GP
(
	id int primary key auto_increment not null,
	name varchar(255) not null,
	practice_id int,
	FOREIGN KEY (practice_id) REFERENCES Practice(id)
);