CREATE TABLE Appointment
(
	id int primary key auto_increment not null,
	appointment_date date not null,
	appointment_time varchar(30) not null,
	gp_id int not null,
	patient_id int not null,
	booked_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp on update current_timestamp,
	FOREIGN KEY (gp_id) REFERENCES GP(id),
	FOREIGN KEY (patient_id) REFERENCES Patient(id)
);