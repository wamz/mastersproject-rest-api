CREATE TABLE Patient
(
	id int primary key auto_increment not null,
	name varchar(255) not null,
	date_of_birth date not null,
	address_line_1 varchar(255) null,
	address_line_2 varchar(255) null,
	town_city varchar(255) null,
	county varchar(255) null,
	postcode varchar(255) not null,
	mobile_number varchar(20) not null,
	telephone_number varchar(20) null,
	email varchar(255) null,
	nhs_number varchar(50) not null,
	password varchar(100) null,
	unique_salt varchar(100) null,
	gp_id int not null,
	FOREIGN KEY (gp_id) REFERENCES GP(id)
);