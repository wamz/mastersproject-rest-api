 CREATE TABLE Prescription
 (
 	id int primary key auto_increment not null,
 	name_of_medication varchar(255) not null,
 	quantity varchar(255) not null,
 	instructions varchar(255) not null,
 	order_number int not null,
 	reorder_times int not null,
 	patient_id int not null,
 	gp_id int not null,
 	date_created timestamp default current_timestamp,
 	FOREIGN KEY (patient_id) REFERENCES Patient(id),
 	FOREIGN KEY (gp_id) REFERENCES GP(id)
 );