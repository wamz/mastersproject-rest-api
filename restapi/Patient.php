<?php
class Patient
{
	private $dbConn;
	
	// constructor
	function __construct(){
		require_once '/DbConnect.php';
		
		// opening database connection
		$db = new DbConnect();
		$this->dbConn = $db->connect();
	}
	
	// function to get Patient's ID
	public function getPatientId($nhsNumber){
		try{
			// Query to retrieve patient id
			$query = $this->dbConn->prepare("SELECT id FROM Patient WHERE nhs_number = :nhsNumber");
			$query->bindParam(':nhsNumber', $nhsNumber);
			
			// Execute query
			if($query->execute()){
				// Retrieve returned patient id and store it in a patient id variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$patientId = $q->id;
				}
				
				// return patient id variable
				return $patientId;
			}
			// Handling exceptions
		}catch (Exception $e){
			return NULL;
		}
	}
	
	// function to validate Patient Identity during patient registration
	public function validatePatientIdentity($nhsNumber, $dateOfBirth, $postcode){
		try{
			// query to retrieve patient name from record with data matching parameters
			$query = $this->dbConn->prepare("SELECT name FROM Patient WHERE nhs_number = :nhsNumber AND	date_of_birth = :dateOfBirth 
				AND postcode = :postcode");
			
			// Executing query
			if($query->execute(array(':nhsNumber' => $nhsNumber, ':dateOfBirth' => $dateOfBirth, ':postcode' => $postcode))){
				$response = array();
				// Retrieving data returned by query and storing it in a response array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$response["name"] = $q->name;
				}
				
				// Returning response array
				return $response;
			}
			// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}

	// function to check if user already has password set
	public function isPasswordExists($nhsNumber, $dateOfBirth, $postcode){
		try{
			// query to retrieve patient name from record with data matching parameters
			$query = $this->dbConn->prepare("SELECT password FROM Patient WHERE nhs_number = :nhsNumber AND	
				date_of_birth = :dateOfBirth AND postcode = :postcode");
			
			// Executing query
			if($query->execute(array(':nhsNumber' => $nhsNumber, ':dateOfBirth' => $dateOfBirth, ':postcode' => $postcode))){
				// Retrieving data returned by query and storing it in a password variable		
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$password = $q->password;
				}
				
				// Checking if password variable is empty or not
				if($password != NULL){
					return TRUE;
				}
			}
			// Handling exceptions
		}catch(Exception $e){
			return FALSE;
		}
	}
	
	// function to create password for patient account
	public function createPassword($nhsNumber, $password){
		try{
			// password hashing
			$salt = Hash::uniqueSalt();
			$passwordHash = Hash::passwordHash($password, $salt);		
			
			// query to update patient record and store new password
			$query = $this->dbConn->prepare("UPDATE Patient SET password = :password_hash, unique_salt = :uniqueSalt WHERE 
				nhs_number = :nhsNumber");
			
			// Execute query
			$query->execute(array(':password_hash' => $passwordHash, ':uniqueSalt' => $salt,':nhsNumber' => $nhsNumber));

			// checking if only one row of data was affected by the update
			if ($query->rowCount() == 1){
				return TRUE;
			}
			// Handling exceptions
		}catch(Exception $e){
			return FALSE;
		}
	}
	
	// Function to handle patient login
	public function login($nhsNumber, $password){
		try{
			// get salt
			$salt = self::getSalt($nhsNumber);
			
			// hash password using salt
			$passwordHash = Hash::passwordHash($password, $salt);
			
			// check validity of nhsNumber and password
			$query = $this->dbConn->prepare("SELECT name FROM Patient WHERE nhs_number = :nhsNumber AND password = :passwordHash");
			
			// Executing query
			if($query->execute(array(':nhsNumber' => $nhsNumber, ':passwordHash' => $passwordHash))){
				// Retrieving data returned by query
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$name = $q->name;
				}
					
				return $name;
			}
			// Handling exceptions
		}catch (Exception $e){
			return NULL;
		}
	}
	
	// function to get Patient's contact details
	public function getContactDetails($patientId){
		try{
			// query to retrieve contact details where nhsNumber matches parameter
			$query = $this->dbConn->prepare("SELECT id, name, address_line_1, address_line_2, town_city, county, postcode, 
				mobile_number, telephone_number, email, gp_id FROM Patient WHERE id = :patientId");
			
			// Execute query
			if($query->execute(array(':patientId' => $patientId))){
				$result = array();
				
				// Retrieve data returned by query and store it in result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["patientId"] = $q->id;
					$result["addressLine1"] = $q->address_line_1;
					$result["addressLine2"] = $q->address_line_2;
					$result["townCity"] = $q->town_city;
					$result["county"] = $q->county;
					$result["postcode"] = $q->postcode;
					$result["mobileNumber"] = $q->mobile_number;
					$result["telephoneNumber"] = $q->telephone_number;
					$result["email"] = $q->email;
					$result["gpId"] = $q->gp_id;
				}
				
				// returning result array
				return $result;
			}
			// Handling exceptions
		}catch (Exception $e){
			return NULL;
		}
	}
	
	// function to update Patient's contact details
	public function editContactDetails($patientId, $addressLine1, $addressLine2, $townCity, $county, $postcode, $telephoneNumber, 
			$mobileNumber, $email){
		try {
			// query to update contact details of patient with nhsNumber matching parameter
			$query = $this->dbConn->prepare("UPDATE Patient SET address_line_1 = :addressLine1,	address_line_2 = :addressLine2,
				town_city = :townCity, county = :county, postcode = :postcode, telephone_number = :telephoneNumber,
				mobile_number = :mobileNumber, email = :email WHERE id = :patientId");
			
			// Execute query
			$query->execute(array(':addressLine1' => $addressLine1, ':addressLine2' => $addressLine2, ':townCity' => $townCity,
					':county' => $county, ':postcode' => $postcode,	':telephoneNumber' => $telephoneNumber, 
					':mobileNumber' => $mobileNumber, ':email' => $email, ':patientId' => $patientId));
			
			// checking if only one row of data was affected by the update
			if ($query->rowCount() == 1){
				return TRUE;
			}
			// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
	
	// function to get unique salt
	private function getSalt($nhsNumber){
		try{
			// Query to retrieve unique salt
			$query = $this->dbConn->prepare("SELECT unique_salt FROM Patient WHERE nhs_number = :nhsNumber");
			$query->bindParam(':nhsNumber', $nhsNumber);
			
			// Execute query
			if ($query->execute()){
				// Retrieve salt returned by query and store it in a salt variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$salt = $q->unique_salt;
				}
				
				// return salt variable
				return $salt;
			}
			// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}

	// Function to check if password is valid
	private function isPasswordValid($nhsNumber, $currentPassword){
		try{
			// get salt from db
			$salt = self::getSalt($nhsNumber);
			
			// hash current password using salt
			$passwordHash = Hash::passwordHash($currentPassword, $salt);

			// Query to retrieve patient id
			$query = $this->dbConn->prepare("SELECT id FROM Patient WHERE password = :passwordHash");

			// Execute query
			if($query->execute(array(':passwordHash' => $passwordHash))){
				// Retrieve patient id returned by query and store it in a patient id variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$patientId = $q->id;
				}

				// return patient id variable
				return $patientId;
			}
			// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}
	
	// function to update Patient password
	public function updatePassword($nhsNumber, $currentPassword, $newPassword){
		try {
			// Checking if current password is valid
			$patientId = self::isPasswordValid($nhsNumber, $currentPassword);

			// Checking if patient id is empty or not
			if ($patientId != NULL) {
				// create new unique salt
				$salt = Hash::uniqueSalt();

				// hashing new password with new salt
				$passwordHash = Hash::passwordHash($newPassword, $salt);
				
				// query to update patient record and store new password
				$query = $this->dbConn->prepare("UPDATE Patient SET password = :password_hash, unique_salt = :salt 
					WHERE id = :patientId");
				// Execute query
				$query->execute(array(':password_hash' => $passwordHash, ':salt' => $salt,':patientId' => $patientId));
				
				// checking if only one row of data was affected by the update
				if ($query->rowCount() == 1){
					return TRUE;
				}
			}
			// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
}