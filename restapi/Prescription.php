<?php
class Prescription
{
	// constructor
	function __construct(){
		require_once '/DbConnect.php';
	
		// opening database connection
		$db = new DbConnect();
		$this->dbConn = $db->connect();
	}
	
	// Function to retrieve all prescriptions related to patient
	public function getAllPrescriptions($patientId){
		try {
			// Query to get all appointments booked by patient from database
			$query = $this->dbConn->prepare("SELECT * FROM Prescription WHERE patient_id = :patientId");
			
			// Executing query
			if($query->execute(array(':patientId' => $patientId))){
				$result = array();
				$prescriptions = array();
				
				// Retrieving all data returned from query, iterating through the data and storing it in a result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["id"] = $q->id;
					$result["nameOfMedication"] = $q->name_of_medication;
					$result["quantity"] = $q->quantity;
					$result["instructions"] = $q->instructions;
					$result["orderNumber"] = $q->order_number;
					$result["reorderTimes"] = $q->reorder_times;
					$result["patientId"] = $q->patient_id;
					$result["gpId"] = $q->gp_id;
										
					// taking iterated data stored in the results array and storing it in the prescriptions array
					array_push($prescriptions, $result);
				}
				
				// returning prescriptions array
				return $prescriptions;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return NULL;
		}
	}

	// Function to check if order number is valid
	private function isOrderNumberValid($orderNumber){
		try{
			// Query to check if order number is valid
			$query = $this->dbConn->prepare("SELECT id FROM Prescription WHERE order_number = :orderNumber");

			// Executing query
			if ($query->execute(array(':orderNumber' => $orderNumber))) {
				// Retrieving id returned by query and storing it in an id variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
						$id = $q->id;
				}

				// Checking id id variable is empty or not
				if ($id != NULL) {
					return TRUE;
				}
			}
		}catch(Exception $e){
			return FALSE;
		}
	}
	
	// Function to reorder a prescription
	public function reorderPrescription($prescriptionId, $nameOfDrug, $quantity, $instructions, $orderNumber, $reorderTimes){
		try {
			// Checking if order number is valid
			if (self::isOrderNumberValid($orderNumber)) {
				// query to update patient record and store password
				$query = $this->dbConn->prepare("UPDATE Prescription SET name_of_medication = :nameOfMedication, quantity = :quantity, 
					instructions = :instructions, reorder_times = :reorderTimes WHERE id = :prescriptionId");

				// Executing query
				$query->execute(array(':nameOfMedication' => $nameOfDrug, ':quantity' => $quantity, ':instructions' => $instructions,
				':reorderTimes' => $reorderTimes, ':prescriptionId' => $prescriptionId));
				
				// checking if only one row of data was affected by the update
				if ($query->rowCount() == 1){
					return TRUE;
				}
			}
		// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
}