<?php
class Hash
{
	// Function to generate a new unique salt
	public static function uniqueSalt(){
		// generate random string and store it in a salt variable
		$salt = substr(mt_rand(), 0, 30);
		// hash salt using SHA256
		$hashSalt = hash('sha256', $salt);
		// return hashed salt
		return $hashSalt;
	}
	
	// Function to hash password
	public static function passwordHash($password, $salt){
		// hash password with hashed salt
		$passwordHash = hash('sha256', $password.$salt);
		// return hashed password
		return $passwordHash;
	}
}