<?php
class DbConnect
{
	private $conn;
	
// 	function __construct(){
// 		self::connect();
// 	}
	
	function connect(){		
		try{
			$host = '127.0.0.1';
			$db = 'mastersproject';
			$user = 'wamz';
			$password = 'chirasha';
			
			// connecting to database
			$this->conn = new PDO("mysql:host=$host;dbname=$db", $user, $password);
			
			// handling exceptions
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			return $this->conn;
		}catch(Exception $e){
			echo 'ERROR: Could Not Connect! <br/>';
			echo $e->getMessage();
		}
	}
	
	function closeConnection(){
		$this->conn = NULL;
	}
}