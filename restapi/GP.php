<?php
class GP
{
	// constructor
	function __construct(){
		require_once '/DbConnect.php';
		
		// opening database connection
		$db = new DbConnect();
		$this->dbConn = $db->connect();
	}

	// function to get GP ID of Patient's GP
	public function getGpId($patientId){
		try{
			// Query to retrieve gp id from Patient table
			$query = $this->dbConn->prepare("SELECT gp_id FROM Patient WHERE id = :patientId");
			$query->bindParam(':patientId', $patientId);
		
			// Execute query
			if($query->execute()){
				// Retrieve gp id returned from query and store it in a gpId variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$gpId = $q->gp_id;
				}
				
				// return gpId variable
				return $gpId;
			}
		// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}
	
	// function to get practiceId of Practice where Patient's GP works
	public function getPracticeId($gpId){
		try{
			// Query to retrieve practice id from GP table
			$query = $this->dbConn->prepare("SELECT practice_id FROM GP WHERE id = :gpId");
			$query->bindParam(':gpId', $gpId);
			
			// Execute query
			if($query->execute()){
				// Retrieve practice id returned from query and store it in a practiceId variable
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$practiceId = $q->practice_id;
				}
				
				// return practiceId variable
				return $practiceId;
			}
		// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}
	
	// function to get all GPs at Practice Patient gets healthcare
	public function getAllGPs($patientId){
		try{
			// Retrieve gpId using patientId
			$gpId = self::getGpId($patientId);
			// Retrieve practiceId using gpId
			$practiceId = self::getPracticeId($gpId);

			// Query to retrieve gp id and name
			$query = $this->dbConn->prepare("SELECT id, name FROM GP WHERE practice_id = :practiceId");
			$query->bindParam(':practiceId', $practiceId);
			
			// Execute query
			if ($query->execute()){
				$result= array();
				$gpDetails = array();
				
				// Retrieve data returned from query and store it in a result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["gpId"] = $q->id;
					$result["gpName"] = $q->name;

					// add result array to gpDetails array
					array_push($gpDetails, $result);
				}
				
				// return gpDetails array
				return $gpDetails;
			}
			// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}

	// function to retrieve GP's name
	public function getGpName($gpId){
		try{
		$query = $this->dbConn->prepare("SELECT name FROM GP WHERE id = :gpId");
		$query->bindParam(':gpId', $gpId);

		if ($query->execute()) {
			while ($q = $query->fetch(PDO::FETCH_OBJ)){
				$name = $q->name;
			}

			return name;
		}
		// Handling exceptions
		}catch(Exception $e){
			return NULL;
		}
	}
}