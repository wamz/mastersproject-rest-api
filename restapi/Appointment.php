<?php
class Appointment
{
	private $dbConn;
	
	// constructor
	function __construct(){
		require_once '/DbConnect.php';
		
		// opening database connection
		$db = new DbConnect();
		$this->dbConn = $db->connect();
	}
	
	// function to create and store new appointment
	public function createAppointment($appointmentDate, $appointmentTime, $gpId, $patientId){
		try {
			// Query to create a new appointment for the patient
			$query = $this->dbConn->prepare("INSERT INTO Appointment (appointment_date, appointment_time, gp_id, patient_id) VALUES
				(:appointmentDate, :appointmentTime, :gpId, :patientId)");
			
			// Executing query
			$result = $query->execute(array(':appointmentDate' => $appointmentDate, ':appointmentTime' => $appointmentTime, ':gpId' => $gpId,
					':patientId' => $patientId));
			
			// Checking if appointment created successfully
			if($result){
				return TRUE;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
	
	// function to check if appointment exists
	public function isAppointmentExists($appointmentDate, $appointmentTime, $gpId, $patientId){
		try {
			// Query to check if appointments exists
			$query = $this->dbConn->prepare("SELECT * FROM Appointment WHERE appointment_date = :appointmentDate AND 
					appointment_time = :appointmentTime AND gp_id = :gpId AND patient_id = :patientId");
			
			// Executing query
			$query->execute(array(':appointmentDate' => $appointmentDate, ':appointmentTime' => $appointmentTime, ':gpId' => $gpId,
					':patientId' => $patientId));
			
			// Checking if a row is returned
			if ($query->rowCount() > 0){
				return TRUE;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
	
	// function to get list of appointments related to Patient
	public function getAllAppointments($patientId){
		try {
			// Query to get all appointments booked by patient from database
			$query = $this->dbConn->prepare("SELECT * FROM Appointment WHERE patient_id = :patientId AND appointment_date >= CURDATE()");
			// Executing query
			if($query->execute(array(':patientId' => $patientId))){
				$result = array();
				$appointments = array();
				// Retrieving data returned by query and storing it in a result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["id"] = $q->id;
					$result["appointmentDate"] = $q->appointment_date;
					$result["appointmentTime"] = $q->appointment_time;
					$result["gpId"] = $q->gp_id;
					$result["patientId"] = $q->patient_id;
					
					// taking iterated data stored in the results array and storing it in the appointments array
					array_push($appointments, $result);
				}
				// returning appointments array
				return $appointments;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return NULL;
		}		
	}
	
	// function to get details on one specific appointment
	public function getAppointment($appointmentId, $patientId){
		try {
			// Query to get a specific appointment frrom Appointment table
			$query = $this->dbConn->prepare("SELECT * FROM Appointment WHERE id = :appointmentId AND patient_id = :patientId");
			// Executing query
			if ($query->execute(array(':appointmentId' => $appointmentId, ':patientId' => $patientId))){
				$result = array();
				// Retrieving appointment data returned by query and storing it in a result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["id"] = $q->id;
					$result["appointmentDate"] = $q->appointment_date;
					$result["appointmentTime"] = $q->appointment_time;
					$result["gpId"] = $q->gp_id;
				}
				// return result array
				return $result;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return NULL;
		}
	}
	
	// function to edit existing appointment
	public function editAppointment($appointmentId, $appointmentDate, $appointmentTime, $gpId, $patientId){
		try {
			// Query to update a specific appointment's details
			$query = $this->dbConn->prepare("UPDATE Appointment SET appointment_date = :appointmentDate, appointment_time = :appointmentTime,
				gp_id = :gpId WHERE id = :appointmentId AND patient_id = :patientId");
			
			// Executing query
			$query->execute(array(':appointmentDate' => $appointmentDate, ':appointmentTime' => $appointmentTime, ':gpId' => $gpId,
					':appointmentId' => $appointmentId, ':patientId' => $patientId));
			
			// Checking if appointment successfully updated
			if ($query->rowCount() == 1){
				return TRUE;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}
	
	// function to delete existing appointment
	public function deleteAppointment($appointmentId, $patientId){
		try {
			// Query to delete an appointment
			$query = $this->dbConn->prepare("DELETE FROM Appointment WHERE id = :appointmentId AND patient_id = :patientId");
			// Executing query
			$query->execute(array(':appointmentId' => $appointmentId, ':patientId' => $patientId));
			// Checking if appointment successfully deleted
			if ($query->rowCount() == 1){
				return TRUE;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return FALSE;
		}
	}

	// function to get list of booked appointment dates and times
	public function getAllBookedTimeSlots(){
		try {
			// Query to get all booked appointment timeslots
			$query = $this->dbConn->prepare("SELECT * FROM Appointment");
			
			// Executing query
			if($query->execute()){
				$result = array();
				$appointments = array();
				// Retrieve data returned by query and store it in a result array
				while ($q = $query->fetch(PDO::FETCH_OBJ)){
					$result["id"] = $q->id;
					$result["appointmentDate"] = $q->appointment_date;
					$result["appointmentTime"] = $q->appointment_time;

					// taking iterated data stored in the results array and storing it in the appointments array
					array_push($appointments, $result);
				}
				// return appointments array
				return $appointments;
			}
		// Handling exceptions
		} catch (Exception $e) {
			return NULL;
		}		
	}
}