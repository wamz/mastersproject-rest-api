<?php
require 'vendor/autoload.php';
require 'restapi/init.php';
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
//require 'Slim/Slim.php';
//\Slim\Slim::registerAutoloader();
/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get('/', function() {
            echo '<h1> Welcome to Slim!</h1><p>';
            echo 'Congratulations! Your Slim application is running';
    }
);

$app->get('/hello/:name', function($name){
	// echo "Hello ". $name;
    $data = array();
    $data["error"] = false;
    $data["message"] = 'Hello '.$name;
    responseToClient(200, $data);
});

/*AUTH ROUTES
---------------------------------------------------------------------*/

$app->post('/validate', function() use ($app){
    $response = array();

    // http post data
    $nhsNumber = $app->request->post('nhsNumber');
    $dateOfBirth = $app->request->post('dateOfBirth');
    $postcode = $app->request->post('postcode');

    $patient = new Patient();

    // validate identity
    $result = $patient->validatePatientIdentity($nhsNumber, $dateOfBirth, $postcode);
    $passwordExists = $patient->isPasswordExists($nhsNumber, $dateOfBirth, $postcode);

    if ($result != null) {
        if(!$passwordExists){
            $response["error"] = false;
            $response["message"] = 'Valid Identity';
            $response["patientName"] = $result["name"];

            responseToClient(200, $response);
        }
        else{
            $response["error"] = true;
            $response["message"] = 'Password Exists, Please Login!';
            responseToClient(200, $response);
        }
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Invalid Identity, Please Try Again!';
        responseToClient(404, $response);
    }
});

$app->post('/register', function() use ($app){
    $response = array();

    // http post data
    $nhsNumber = $app->request->post('nhsNumber');
    $password = $app->request->post('password');

    $patient = new Patient();

    $patientId = $patient->getPatientId($nhsNumber);
    $registration = $patient->createPassword($nhsNumber, $password);

    if ($patientId != null && $registration) {
        $response["error"] = false;
        $response["message"] = 'Successful Registration!';
        $response["patientId"] = $patientId;
        responseToClient(201, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Unsuccessful Registration!';
        responseToClient(400, $response);
    }
});

$app->post('/login', function() use ($app){
    $response = array();

    // http post data
    $nhsNumber = $app->request->post('nhsNumber');
    $password = $app->request->post('password');

    $patient = new Patient();

    $patientId = $patient->getPatientId($nhsNumber);
    $loginResult = $patient->login($nhsNumber, $password);

    if ($patientId != null && $loginResult != null){
        $response["error"] = false;
        $response["message"] = 'You are logged in!';
        $response["patientId"] = $patientId;
        $response["patientName"] = $loginResult;
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Invalid Login Details!';
        responseToClient(404, $response);
    }
});

// Route pass all relevant data to app
$app->get('/dashboard', function(){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    $patient = new Patient();
    $appointment = new Appointment();
    $gp = new GP();
    $prescription = new Prescription();

    $patientDetails = $patient->getContactDetails($patientId);
    $appointments = $appointment->getAllAppointments($patientId);
    $gpDetails = $gp->getAllGPs($patientId);
    $prescriptions = $prescription->getAllPrescriptions($patientId);

    if ($patientDetails != null || $appointments != null || $gpDetails != null) {
            $response["error"] = false;
            $response["patientDetails"] = $patientDetails;
            $response["appointments"] = $appointments;
            $response["gpDetails"] = $gpDetails;
            $response["prescriptions"] = $prescriptions;
            responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointments Not Found!';
        responseToClient(404, $response);
    }
});

/*PROFILE ROUTES
---------------------------------------------------------------------*/
// Route to get patient contact details
$app->get('/profile/details', function(){
    $app = \Slim\Slim::getInstance();
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    $patient = new Patient();

    $details = $patient->getContactDetails($patientId);

    if ($details != null) {
        $response["error"] = false;
        $response["Contact Details"] = $details;
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Contact Details Not Found!';
        responseToClient(404, $response);
    }
});

// Route to update patient contact details
$app->post('/profile/update', function() use ($app){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    // http post data
    $addressLine1 = $app->request->post('addressLine1');
    $addressLine2 = $app->request->post('addressLine2');
    $townCity = $app->request->post('townCity');
    $county = $app->request->post('county');
    $postcode = $app->request->post('postcode');
    $mobileNumber = $app->request->post('mobileNumber');
    $telephoneNumber = $app->request->post('telephoneNumber');
    $email = $app->request->post('email');

    $patient = new Patient();

    $result = $patient->editContactDetails($patientId, $addressLine1, $addressLine2, $townCity, $county, $postcode, $telephoneNumber, 
            $mobileNumber, $email);

    if ($result){
        $response["error"] = false;
        $response["message"] = 'Contact Details Successfully Updated!';
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Contact Details Not Updated!';
        responseToClient(404, $response);
    }
});

// Route to update password
$app->post('/profile/security', function() use ($app){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $nhsNumber = $headers['Authorization'];

    // http post data
    $currentPassword = $app->request->post('currentPassword');
    $newPassword = $app->request->post('newPassword');

    $patient = new Patient();

    $result = $patient->updatePassword($nhsNumber, $currentPassword, $newPassword);

    if ($result){
        $response["error"] = false;
        $response["message"] = 'Password Successfully Updated!';
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Password Not Updated!';
        responseToClient(404, $response);
    }
});

/*APPOINTMENT ROUTES
---------------------------------------------------------------------*/

// Route to get all booked timeslots
$app->get('/timeslots', function(){
    $response = array();

    $appointment = new Appointment();

    $appointments = $appointment->getAllBookedTimeSlots();

    if ($appointments != null) {
            $response["error"] = false;
            $response["appointments"] = $appointments;
            responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Time Slots Not Found!';
        responseToClient(404, $response);
    }
});

// ROute to create a new appointment
$app->post('/appointment/create', function() use ($app){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    // http post data
    $appointmentDate = $app->request->post('appointmentDate');
    $appointmentTime = $app->request->post('appointmentTime');
    $gpId = $app->request->post('gpId');

    $appointment = new Appointment(); 

    // Checking if appointment doesn't exist
    if (!$appointment->isAppointmentExists($appointmentDate, $appointmentTime, $gpId, $patientId)) {
        // Creating appointment
        $result = $appointment->createAppointment($appointmentDate, $appointmentTime, $gpId, $patientId);

        if ($result){
            $response["error"] = false;
            $response["message"] = 'Appointment Created!';
            responseToClient(201, $response);
        }
        else{
            $response["error"] = true;
            $response["message"] = 'Error: Appointment Not Created! <p> Please try again later';
            responseToClient(400, $response);
        }
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointment Already Exists!';
        responseToClient(200, $response);
    }
});

// Route to get all appointments booked by patient
$app->get('/appointments', function(){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    $appointment = new Appointment();
    $gp = new GP();

    $appointments = $appointment->getAllAppointments($patientId);
    $gpDetails = $gp->getAllGPs($patientId);

    if ($appointments != null) {
            $response["error"] = false;
            $response["message"] = 'Appointments Found!';
            $response["appointments"] = $appointments;
            $response["gpDetails"] = $gpDetails;
            responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointments Not Found!';
        responseToClient(404, $response);
    }
});

// ROute to get details of one specific appointment for patient
$app->get('/appointment/details/:id', function($appointmentId){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    $appointment = new Appointment();

    $result = $appointment->getAppointment($appointmentId, $patientId);

    if ($result != null) {
        $response["error"] = false;
        $response["message"] = 'Appointment Found!';
        $response["appointment"] = $result;
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointment Not Found!';
        responseToClient(404, $response);
    }
});

// Route to edit an appointment
$app->post('/appointment/update/:id', function($appointmentId) use ($app){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    // http post data
    $appointmentDate = $app->request->post('appointmentDate');
    $appointmentTime = $app->request->post('appointmentTime');
    $gpId = $app->request->post('gpId');

    $appointment = new Appointment();

    $result = $appointment->editAppointment($appointmentId, $appointmentDate, $appointmentTime, $gpId, $patientId);

    if ($result) {
        $response["error"] = false;
        $response["message"] = 'Appointment Successfully Updated!';
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointment Not Found!';
        responseToClient(404, $response);
    }
});

// ROute to delete an appointment
$app->delete('/appointment/:id', function($appointmentId){
    $response = array();

    // getting the request headers
    $headers = apache_request_headers();
    $patientId = $headers['Authorization'];

    $appointment = new Appointment();

    $result = $appointment->deleteAppointment($appointmentId, $patientId);

    if ($result) {
        $response["error"] = false;
        $response["message"] = 'Appointment Successfully Cancelled!';
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Appointment Not Found!';
        responseToClient(404, $response);
    }
});

/*PRESCRIPTION ROUTES
---------------------------------------------------------------------*/

// Route to reorder a prescription
$app->post('/prescription/reorder/:id', function($prescriptionId) use ($app){
    $response = array();

    // http post data
    $nameOfMedication = $app->request->post('nameOfMedication');
    $quantity = $app->request->post('quantity');
    $instructions = $app->request->post('instructions');
    $orderNumber = $app->request->post('orderNumber');
    $reorderTimes = $app->request->post('reorderTimes');

    $prescription = new Prescription();

    $result = $prescription->reorderPrescription($prescriptionId, $nameOfMedication, $quantity, $instructions, $orderNumber, $reorderTimes);

    if ($result) {
        $response["error"] = false;
        $response["message"] = 'Prescription Successfully Re-ordered!';
        responseToClient(200, $response);
    }
    else{
        $response["error"] = true;
        $response["message"] = 'Error: Prescription Not Re-ordered!';
        responseToClient(404, $response);
    }
});

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);
// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});
// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

function responseToClient($statusCode, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->response->setStatus($statusCode);

    // setting response content type to json
    $app->response->headers->set('Content-Type', 'application/json');

    echo json_encode($response);
}

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();